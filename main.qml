import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: rootWin
    visible: true
    width: 1280
    height: 800
    title: qsTr("Hard Tissue Charting")
    property int currentTooth: 2

    function isPosterior() {
            var returnMe = true;

            if((currentTooth >= 6) && (currentTooth <= 11)) { //maxillary anterior
                returnMe = false;
            } else if((currentTooth >= 22) && (currentTooth <= 27)) { //mandibular anterior
                returnMe = false;
            }

            return returnMe;
        }


    onCurrentToothChanged: {
        amalgamSurfaces.clearSurfaces();
        compSurfaces.clearSurfaces();
        exPfmButton.checked = false;
        exZirButton.checked = false;
        exGoldButton.checked = false;
        exOtherButton.checked = false;
        decaySurfaces.clearSurfaces();
        fractureSurfaces.clearSurfaces();
        otherBox.checked = false;
        watchSurfaces.clearSurfaces();
        compositeFilling.checked = false;
        amalgamFilling.checked = false;
        onlay.checked = false;
        goldCrown.checked = false;
        pfmCrown.checked = false;
        zirCrown.checked = false;
        rctBox.checked = false;
    }

    Column {
        id: chartCol
        anchors.left: parent.left
        width: parent.width/2
        spacing: 4

        Label {
            text:"Current Tooth: #" + currentTooth
            font.pointSize: 24

        }
        Button {
            text: "Missing"
            width: parent.width
            onClicked: if(currentTooth<32){currentTooth++;}
        }
        Label {
            text: "Existing"
            font.pointSize: 16
            width: parent.width
            background: Rectangle {
                color: "grey"
            }
            horizontalAlignment: Text.AlignHCenter
        }
        Row {
            spacing: 5
            Label {
                text: "Amalgam"
                font.pointSize: 16
                height: parent.height
                verticalAlignment: Text.AlignVCenter
            }
            PostSurfaceChoose {
                id: amalgamSurfaces
            }
        }

        Row {
            spacing: 5
            Label {
                text: "Composite"
                font.pointSize: 16
                height: parent.height
                verticalAlignment: Text.AlignVCenter
            }
            PostSurfaceChoose {
                id: compSurfaces
            }
        }

        Row {
            spacing: 5
            Label {
                text: "Crown"
                font.pointSize: 16
                height: parent.height
                verticalAlignment: Text.AlignVCenter
            }
            RadioButton {
                id: exPfmButton
                text: "PFM"
                onPressAndHold:checked = false;
            }
            RadioButton {
                id: exZirButton
                text: "Full Ceramic/Zirconia"
                onPressAndHold:checked = false;
            }
            RadioButton {
                id: exGoldButton
                text: "Gold"
                onPressAndHold:checked = false;
            }
            RadioButton {
                id: exOtherButton;
                text: "Other"
                onPressAndHold:checked = false;
            }
        }

        Label {
            text: " "
            font.pointSize: 4
            width: parent.width
        }

        Label {
            text: "Conditions"
            font.pointSize: 16
            width: parent.width
            background: Rectangle {
                color: "grey"
            }
            horizontalAlignment: Text.AlignHCenter
        }

        Row {
            spacing: 5
            Label {
                text: "Decay"
                font.pointSize: 16
                height: parent.height
                verticalAlignment: Text.AlignVCenter
            }
            PostSurfaceChoose {
                id: decaySurfaces
                property string decaySur: decaySurfaces.getSurfaces()
                onSurfaceChanged: decaySur = newSurfaces
            }
        }

        Row {
            spacing: 5
            Label {
                text: "Fracture"
                font.pointSize: 16
                height: parent.height
                verticalAlignment: Text.AlignVCenter
            }
            PostSurfaceChoose {
                id: fractureSurfaces
                property string fractSur: fractureSurfaces.getSurfaces()
                onSurfaceChanged: fractSur = newSurfaces
            }
        }
        Row {
            CheckBox {
                id: otherBox
                text: "Other"
            }
            TextField {
                id: otherField
                opacity: otherBox.checked ? 1:0
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }

            }
        }

    }

    Label {
        id: dateLabel
        font.pointSize: 16
        text: "1/1/2019"
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        anchors.top:parent.top
        anchors.left: chartCol.right
        anchors.right: parent.right
        anchors.margins: 4
    }


    Image {
        id: bitewingRad
        z: 98
        source: {
            if(isPosterior()) {
                if((currentTooth < 4) || (currentTooth > 29)) {
                    return "file:///home/tshah/patData/Test_Patient[10-10-1970]/images/radiograph/1-1-2019/BW/RD.JPG";
                }
                else if((currentTooth < 6) || (currentTooth > 27)) {
                    return "file:///home/tshah/patData/Test_Patient[10-10-1970]/images/radiograph/1-1-2019/BW/RM.JPG";
                }
                else if((currentTooth < 14) || (currentTooth > 19)) {
                    return "file:///home/tshah/patData/Test_Patient[10-10-1970]/images/radiograph/1-1-2019/BW/LM.JPG";
                }
                else {
                    return "file:///home/tshah/patData/Test_Patient[10-10-1970]/images/radiograph/1-1-2019/BW/LD.JPG";
                }

            } else {
                //return "";
            }
        }

        opacity: isPosterior() ? 1 : 0

        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }

        anchors.top:dateLabel.bottom
        anchors.left: chartCol.right
        anchors.right: parent.right
        anchors.bottom: treatmentPlanRect.top
        anchors.margins: 4

        fillMode: Image.PreserveAspectFit

        MouseArea {
            anchors.fill: bitewingRad
            onClicked:  {bitewingRad.state == 'fullscreen' ?
                             bitewingRad.state = "" : bitewingRad.state = 'fullscreen'}
            onPressAndHold:{breather.running = !breather.running;}
        }

        states: [State {
                name: "fullscreen"
                AnchorChanges {
                    target: bitewingRad;
                    anchors.left: rootWin.contentItem.left
                    anchors.bottom: rootWin.contentItem.bottom
                    anchors.top: rootWin.contentItem.top
                }
            }]

        transitions: Transition {
            AnchorAnimation {duration: 500;easing.type: Easing.InOutQuad}
        }
    }

    Label {
        text: "No radiograph available for this tooth";
        anchors.fill: bitewingRad
        opacity: isPosterior() ? 0 :1
        font.pointSize: 16
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }

    BrightnessContrast {
        id: effectRad
        visible: breather.running
        anchors.fill: bitewingRad
        source: bitewingRad
        brightness: 0
        contrast: 0
        z: 99

        SequentialAnimation {
            id: breather
            loops: Animation.Infinite
            PropertyAnimation {
                target: effectRad
                property: "contrast"
                from: -.2
                to: .2
                duration: 2000
            }
            PropertyAnimation {
                target: effectRad
                property: "contrast"
                from: .2
                to: -.2
                duration: 2000
            }
        }
    }

    Rectangle {
        id: treatmentPlanRect
        anchors.top:chartCol.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 4
        width: parent.width * .60
        color: Qt.rgba(.5,0,0,.05)
        Column {
            width: parent.width

            move: Transition {NumberAnimation { properties: "x,y"; duration: 200 }}

            ButtonGroup {
                  buttons:[compositeFilling, amalgamFilling, onlay, goldCrown, pfmCrown, zirCrown]
            }

            Label {
                id: treatmentPlanLabel
                text: "Treatment Plan"
                width: parent.width
                font.pointSize: 16
                horizontalAlignment: Text.AlignHCenter
            }

            Row {
                id: watchRow
                spacing: 5
                Label {
                    text: "Watch"
                    font.pointSize: 12
                    height: compositeFilling.height
                    verticalAlignment: Text.AlignVCenter
                }
                PostSurfaceChoose {
                    id: watchSurfaces

                }
            }

            Row {
                id: fillingRow
                opacity: ((decaySurfaces.decaySur.length>0) || (fractureSurfaces.fractSur.length>0))
                         ? 1:0
                visible: opacity > 0
                Behavior on opacity {
                    PropertyAnimation {duration: 200}
                }

                Label {
                    id: fillLabel
                    text: "Filling"
                    font.pointSize: 12
                    height: compositeFilling.height
                    verticalAlignment: Text.AlignVCenter
                }
                RadioButton {
                    id: compositeFilling
                    text:  decaySurfaces.decaySur.length > fractureSurfaces.fractSur.length ?
                               decaySurfaces.decaySur +" composite" :
                               fractureSurfaces.fractSur +" composite"
                    onPressAndHold:checked = false;
                }

                RadioButton {
                    id: amalgamFilling
                    text:  decaySurfaces.decaySur.length > fractureSurfaces.fractSur.length ?
                               decaySurfaces.decaySur +" amalgam" :
                               fractureSurfaces.fractSur +" amalgam"
                    onPressAndHold:checked = false;
                }

                RadioButton {
                    id: onlay
                    text:  decaySurfaces.decaySur.length > fractureSurfaces.fractSur.length ?
                               decaySurfaces.decaySur +" gold onlay" :
                               fractureSurfaces.fractSur +" gold onlay"
                    onPressAndHold:checked = false;
                }
            }

            Row {
                id: crownRow
                opacity: ((decaySurfaces.decaySur.length>2) || (fractureSurfaces.fractSur.length>0)
                          || (rctBox.checked))
                         ? 1:0
                visible: opacity > 0

                Behavior on opacity {
                    PropertyAnimation {duration: 200}
                }

                Label {
                    text: "Crown"
                    font.pointSize: 12
                    height: compositeFilling.height
                    verticalAlignment: Text.AlignVCenter
                }
                RadioButton {
                    id: goldCrown
                    text:  "Gold Crown"
                    onPressAndHold:checked = false;
                }
                RadioButton {
                    id: pfmCrown
                    text:  "PFM Crown"
                    onPressAndHold:checked = false;
                }
                RadioButton {
                    id: zirCrown
                    text:  "Ceramic/Zirconia Crown"
                    onPressAndHold:checked = false;
                }
            }

            Row {
                id: rctRow
                CheckBox {
                    id: rctBox
                    text: "RCT"
                }
            }
        }
    }

    Rectangle {
        id: extraInfoRect
        anchors.top:chartCol.bottom
        anchors.bottom: toothButtonRow.top
        anchors.left: treatmentPlanRect.right
        anchors.right: parent.right
        anchors.margins: 4
        color: Qt.rgba(0,0,.5,.05)

        Column {
            width: parent.width
            Label {
                text: "Periodontal Pockets (1/1/2019)"
                font.pointSize: 16
                horizontalAlignment: Text.AlignHCenter
                width: parent.width
            }
            GridLayout {
                columns: 5
                rows: 4
                anchors.horizontalCenter: parent.horizontalCenter
                Label {
                    Layout.columnSpan: 5
                    text: "Buccal"
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignHCenter
                }
                Label {
                    Layout.rowSpan: 2
                    text: "Mesial"
                }

                Label {
                    text: "3"
                    color: "green"
                }
                Label {
                    text: "2"
                    color: "green"
                }
                Label {
                    text: "3"
                    color: "green"
                }

                Label {
                    Layout.rowSpan: 2
                    text: "Distal"
                }

                Label {
                    text: "2"
                    color: "green"
                }
                Label {
                    text: "2"
                    color: "green"
                }
                Label {
                    text: "1"
                    color: "green"
                }

                Label {
                    Layout.columnSpan: 5
                    text: "Lingual"
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignHCenter
                }
            }
            MenuSeparator {width: parent.width}

            Label {
                text: "Previous Treatment"
                font.pointSize: 16
                horizontalAlignment: Text.AlignHCenter
                width: parent.width
            }

            Label {
                text: "None"
                horizontalAlignment: Text.AlignHCenter
                width: parent.width
            }
        }

    }

    RowLayout {
        id: toothButtonRow
        anchors.bottom: parent.bottom
        anchors.left: treatmentPlanRect.right
        anchors.right: parent.right
        anchors.margins: 4
        spacing: 4
        Button {
            text: "Previous Tooth"
            icon.name: "go-previous"
            Layout.fillWidth: true
            enabled: currentTooth > 1
            onClicked: {
                currentTooth--;

            }
        }
        Button {
            text: "Next Tooth"
            icon.name: "go-next"
            Layout.fillWidth: true
            enabled: currentTooth < 32
            onClicked: {
                currentTooth++;
            }
        }
    }

    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: rootWin.height
        width: rootWin.width

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: rootWin.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }
}
