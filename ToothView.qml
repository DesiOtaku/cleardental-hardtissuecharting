import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Scene3D 2.0

import Qt3D.Core 2.9
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

Scene3D {
    aspects: ["input", "logic"]
    Entity {
        Camera {
            id: camera
            projectionType: CameraLens.PerspectiveProjection
            fieldOfView: 45
            nearPlane : 0.1
            farPlane : 1000.0
            position: Qt.vector3d( 0.0, 00.0, 40.0 )
            upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
            viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
        }
        FirstPersonCameraController { camera: camera }

        components: [
            RenderSettings {
                activeFrameGraph: ForwardRenderer {
                    camera: camera
                    clearColor: "transparent"
                }
            },
            InputSettings { }
        ]

        PhongMaterial {
            id: material
            diffuse: "white"
        }

        Entity {
            DirectionalLight {
                id: directional
                worldDirection: Qt.vector3d(0.3, 1.0, -5.0).normalized();
                color: "#fffff0"
                intensity: .88
            }
            Transform {
                id: lightpostransform
                translation: Qt.vector3d(0.0, -50.0, -60.0)
            }
            components: [lightpostransform, directional]
        }


        Mesh {
            id: torusMesh
            source: "qrc:/3dmodels/tooth2.obj"
        }

        Transform {
            id: torusTransform
            rotation: fromAxisAndAngle(Qt.vector3d(1, 0, 0), 00)
        }

        Entity {
            id: torusEntity
            components: [ torusMesh, material, torusTransform ]
        }


        NumberAnimation {
            target: torusTransform
            property: "rotationY"
            duration: 10000
            from: 0
            to: 360

            loops: Animation.Infinite
            running: true
        }

        NumberAnimation {
            target: torusTransform
            property: "rotationX"
            duration: 100000
            from: 0
            to: 360

            loops: Animation.Infinite
            running: true
        }

    }
}
