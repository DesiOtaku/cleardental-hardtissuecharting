import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0

Column {
    property var choosenSurfaces: getSurfaces();

    function getSurfaces() {
        var returnMe="";
        if(mSurface.checked) {
            returnMe +="M";
        } if(iSurface.checked) {
            returnMe +="I";
        } if(dSurface.checked) {
            returnMe +="D";
        } if(fSurface.checked) {
            returnMe +="F";
        }if(lSurface.checked) {
            returnMe +="L";
        }
        return returnMe;
    }

    function clearSurfaces() {
        mSurface.checked = false;
        iSurface.checked = false;
        dSurface.checked = false;
        fSurface.checked = false;
        lSurface.checked = false;
    }

    Row {
        RoundButton {
            id: mSurface
            text: "M"
            width: font.pixelSize  *3
            checkable: true
        }
        RoundButton {
            id: iSurface
            text: "I"
            width: font.pixelSize  *3
            checkable: true
        }
        RoundButton {
            id: dSurface
            text: "D"
            width: font.pixelSize  *3
            checkable: true
        }
        RoundButton {
            id: fSurface
            text: "F"
            width: font.pixelSize  *3
            checkable: true
        }
        RoundButton {
            id: lSurface
            text: "L"
            width: font.pixelSize  *3
            checkable: true
        }
    }

}
