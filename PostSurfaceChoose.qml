import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0

Column {
    property var choosenSurfaces: getSurfaces();
    signal surfaceChanged(string newSurfaces)

    function getSurfaces() {
        var returnMe="";
        if(mSurface.checked) {
            returnMe +="M";
        } if(iSurface.checked) {
            returnMe +="O";
        } if(dSurface.checked) {
            returnMe +="D";
        } if(fSurface.checked) {
            returnMe +="B";
        }if(lSurface.checked) {
            returnMe +="L";
        }
        return returnMe;
    }

    function clearSurfaces() {
        mSurface.checked = false;
        iSurface.checked = false;
        dSurface.checked = false;
        fSurface.checked = false;
        lSurface.checked = false;
    }

    Row {
        spacing: 3
        RoundButton {
            id: mSurface
            text: "M"
            width: font.pixelSize  *3
            checkable: true
            onCheckedChanged: surfaceChanged(getSurfaces())
        }
        RoundButton {
            id: iSurface
            text: "O"
            width: font.pixelSize  *3
            checkable: true
            onCheckedChanged: surfaceChanged(getSurfaces())
        }
        RoundButton {
            id: dSurface
            text: "D"
            width: font.pixelSize  *3
            checkable: true
            onCheckedChanged: surfaceChanged(getSurfaces())
        }
        RoundButton {
            id: fSurface
            text: "B"
            width: font.pixelSize  *3
            checkable: true
            onCheckedChanged: surfaceChanged(getSurfaces())
        }
        RoundButton {
            id: lSurface
            text: "L"
            width: font.pixelSize  *3
            checkable: true
            onCheckedChanged: surfaceChanged(getSurfaces())
        }
    }

}
